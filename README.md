# AutohidePropertyWindow VS Extension
Visual Studio Plugin for auto-hiding property window when working with code files

* Hides property window while editing code files
* Shows property window when using a designer window

Works __only__ for __English__ and __German__ Visual Studio languages